section .text
 %define EXIT 60
 %define NULL_TERMINATOR 0
 %define SYS_CALL 1
 %define STDOUT 1
 %define new_string 0xA
 %define MIN 0x30
 %define MAX 0x39
 %define SPACE 0x20
 %define TAB 0x09
 
; Принимает код возврата и завершает текущий процесс
exit: 
     mov rax, EXIT
     syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    .loop:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], NULL_TERMINATOR
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, SYS_CALL
    mov  rdi, STDOUT
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rax, rdi
    dec rsp ; Резервируем место в стеке
    mov byte[rsp], al
    mov rsi, rsp
    mov rdi, STDOUT
    mov rax, SYS_CALL
    mov rdx, 1
    syscall
    inc rsp ; Освобождаем место в стеке
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, new_string
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, rsp
    dec rsp 
    mov byte[rsp], 0
    .loop:
    	xor rdx, rdx
    	xor r10, r10
    	mov r11, 0x0A
    	div r11
    	add dx, 0x0030
    	mov r10, rax
    	mov rax, rdx
    	dec rsp
    	mov byte[rsp], al
    	mov rax, r10
    	test rax, rax
    	jnz .loop
    .out:
    	mov rdi, rsp
        push rdi
        push rax
    	call print_string
        pop rax
        pop rdi
    	mov rdi, rsp
        push rdi
    	call string_length
        pop rdi
    	add rsp, rax
    	inc rsp
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jl .n
    .p:
        jmp print_uint
    .n:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    .A:
        mov r8b, byte[rdi]
        mov r9b, byte[rsi]
        cmp r8b, r9b
        jne .C
        test r8b, r8b
        je .B
        inc rdi
        inc rsi
        jmp .A
    .B:
        mov rax, 1
        ret
    .C:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp

    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor r9, r9
    .loop:
        push r9
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop r9
        
        test rax, rax
        je .quit

        cmp al, SPACE
        je .skip
        cmp al, TAB
        je .skip
        cmp al, new_string
        je .skip

        mov [rdi+r9], rax
        inc r9
        cmp r9, rsi
        jge .err

        jmp .loop
    .skip:
        test r9, r9
        je .loop
        jmp .quit
    .err:
        xor rax, rax
        xor rdx, rdx
        ret
    .quit:
        xor rax, rax
        mov [rdi+r9], rax
        mov rax, rdi
        mov rdx, r9
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rax, rax
mov rcx, rdi
mov r11, 10
    .A:
      movzx r8, byte[rdi]

      cmp r8, 0x0
      je .B
      cmp r8b, MIN
      jl .B
      cmp r8b, MAX
      jg .B

      mul r11
      sub r8, MIN
      add rax, r8
      inc rdi
      jmp .A
    .B:
      sub rcx, rdi
      neg rcx
      mov rdx, rcx
      ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .n
    jmp .p
    .p:
        jmp parse_uint
    .n:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r9, r9 ; r9 contains offset

    push rsi 
    push rdi
    push r9 
    push rdx
    call string_length
    pop rdx
    pop r9 
    pop rdi
    pop rsi

    mov r10, rax; r10 contains len
    cmp rdx, r10
    jl .err

    .loop:
        cmp r9, r10
        jg .end

        mov r8, [rdi+r9] ;moving from the string
        mov [rsi+r9], r8 ;to the buffer
        
        inc r9
        jmp .loop
    .err:
        xor rax, rax
        ret
    .end:
        mov rax, r10
        ret
